﻿using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public Transform StartPosition; //Desde donde se empieza la búsqueda
    public LayerMask WallMask; //Layer de los muros, (se tendrá en cuenta para calcular el terreno recorrible) 
    public Vector2 gridWorldSize; //El tamaño del grid (ancho y atlo)
    public float nodeRadius; //Tamaño de los nodos (cuadraditos)
    public float Distance; //Distancia entre nodos
    //Prefabs visuales para los colores del grid
    public Transform prefabWhite;
    public Transform prefabRed;
    Node[,] grid; //Nuestro array de nodos
    public List<Node> FinalPath; //Lista de nodos finales que se recorrerán

    float nodeDiameter; //Diametro de los nodos (Distance * 2)
    int gridSizeX, gridSizeY; //Tamaño del grid (x e y)

    private void Awake()
    {
        nodeDiameter = nodeRadius * 2; //Calculamos el diámetro
        //Dividimos por el diametro x e y
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        //Creamos el grid
        CreateGrid();
        DrawGrid();
    }

    void CreateGrid()
    {
        //Creamos la variable grid
        grid = new Node[gridSizeX, gridSizeY];
        //Calculamos la posicion del la parte inferior izquierda del grid (desde donde empezaremos)
        Vector3 bottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;
        //Recorremos todo el grid y con un "CheckSphere" comprobamos si es cada casilla corresponde a un muro o no
        for (int y = 0; y < gridSizeY; y++)
        {
            for (int x = 0; x < gridSizeX; x++)
            {
                Vector3 worldPoint = bottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                bool Wall = true;
                if (Physics.CheckSphere(worldPoint, nodeRadius, WallMask))
                {
                    Wall = false;
                }
                grid[x, y] = new Node(Wall, worldPoint, x, y);
            }
        }
    }

    //Esta función devuelve el nodo más cercano a la posición dada en el mundo
    public Node NodeFromWorldPosition(Vector3 a_WorldPosition)
    {
        float xpoint = ((a_WorldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x);
        float ypoint = ((a_WorldPosition.z + gridWorldSize.y / 2) / gridWorldSize.y);

        xpoint = Mathf.Clamp01(xpoint);
        ypoint = Mathf.Clamp01(ypoint);

        int x = Mathf.RoundToInt((gridSizeX - 1) * xpoint);
        int y = Mathf.RoundToInt((gridSizeY - 1) * ypoint);
        //Devolvemos la casilla más cercana
        return grid[x, y];
    }

    //Esta función devuelve una lista de nodos "vecinos" al nodo actual.
    public List<Node> GetNeighboringNodes(Node a_Node)
    {
        //Creamos una lista de nodos "vecinos"
        List<Node> NeighBoringNodes = new List<Node>();
        int xCheck;
        int yCheck;
        //Comprobamos la posición a la derecha (gridX + 1)
        xCheck = a_Node.gridX + 1;
        yCheck = a_Node.gridY;
        if (xCheck >= 0 && xCheck < gridSizeX)
        {
            if (yCheck >= 0 && yCheck < gridSizeY)
            {
                NeighBoringNodes.Add(grid[xCheck, yCheck]);
            }
        }
        //Comprobamos la posición a la izquierda (gridX - 1)
        xCheck = a_Node.gridX - 1;
        yCheck = a_Node.gridY;
        if (xCheck >= 0 && xCheck < gridSizeX)
        {
            if (yCheck >= 0 && yCheck < gridSizeY)
            {
                NeighBoringNodes.Add(grid[xCheck, yCheck]);
            }
        }
        //Comprobamos la posición encima (gridy + 1)
        xCheck = a_Node.gridX;
        yCheck = a_Node.gridY + 1;
        if (xCheck >= 0 && xCheck < gridSizeX)
        {
            if (yCheck >= 0 && yCheck < gridSizeY)
            {
                NeighBoringNodes.Add(grid[xCheck, yCheck]);
            }
        }
        //Comprobamos la posición debajo (gridy - 1)
        xCheck = a_Node.gridX;
        yCheck = a_Node.gridY - 1;
        if (xCheck >= 0 && xCheck < gridSizeX)
        {
            if (yCheck >= 0 && yCheck < gridSizeY)
            {
                NeighBoringNodes.Add(grid[xCheck, yCheck]);
            }
        }
        //Devolvemos una lista de nodos "vecinos"
        return NeighBoringNodes;
    }

    //Función meramente visual
    private void DrawGrid()
    {
        if (grid != null)
        {
            foreach (Node n in grid)
            {
                if (n.IsWall)
                {
                    //Dibujasmos las casillas circulables
                    Instantiate(prefabWhite, n.Position, Quaternion.identity.normalized);
                }
                else
                {
                    //Dibujasmos los muros
                    Instantiate(prefabRed, n.Position, Quaternion.identity.normalized);
                }
            }
        }
    }
}
