﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Pathfinding : MonoBehaviour
{
    PathRequestManager requestManager;
    Grid grid;

    private void Awake()
    {
        //Referencia al requestmanager
        requestManager = GetComponent<PathRequestManager>();
        //Referencia al grid
        grid = GetComponent<Grid>();
    }

    //Llama a la Corutina FindPath Con los valores dados (optimización)
    public void StartFindPath(Vector3 startPos, Vector3 targetPos)
    {
        StartCoroutine(FindPath(startPos, targetPos));
    }

    //Implementacion del algoritmo principal del A*
    IEnumerator FindPath(Vector3 a_StartPos, Vector3 a_TargetPos)
    {
        Vector3[] waypoints = new Vector3[0]; //Waypoints finales 
        bool pathSuccess = false; //Booleano que nos dira si hemos llegado o no
        Node StartNode = grid.NodeFromWorldPosition(a_StartPos); //Nodo mas cercano a la posición inicial
        Node TargetNode = grid.NodeFromWorldPosition(a_TargetPos); //Nodo mas cercano a la posición final

        List<Node> OpenList = new List<Node>(); //Lista de nodos a de la lista abierta
        HashSet<Node> ClosedList = new HashSet<Node>(); //HashSet de nodos de la lista cerrada
        //Los HashSets son parecidos a una lista, pero eliminan duplicados y aumenta el rendimiento

        //Añadimos el nodo inicial a la lista abierta
        OpenList.Add(StartNode);

        //Mientras que haya elementos en la lista abierta
        while (OpenList.Count > 0)
        {
            Node CurrentNode = OpenList[0]; //Añadir el nodo actual a la lista
            for (int i = 1; i < OpenList.Count; i++) //recorremos la lista empezando por el segundo nodo
            {
                //Si el coste F de ese nodo es menor o igual al coste F del nodo actual
                if (OpenList[i].FCost < CurrentNode.FCost || OpenList[i].FCost == CurrentNode.FCost &&
                                                             OpenList[i].hCost < CurrentNode.hCost)
                {
                    //Añadimos el nodo actual a la lista abierta
                    CurrentNode = OpenList[i];
                }
            }
            //Lo quitamos de la lista abierta y lo metemos en la cerrada (nodo final)
            OpenList.Remove(CurrentNode);
            ClosedList.Add(CurrentNode);
            //Si el nodo que metemos es el nodo final habremos completado el pathfinding
            if (CurrentNode == TargetNode)
            {
                pathSuccess = true;
                break; //salimos del while (TODO: mejorar con un bool?)
            }

            //Analizamos los nodos colindantes
            foreach (Node NeighborNode in grid.GetNeighboringNodes(CurrentNode))
            {
                //Si es un muro o ya ha sido analizado (pertenece a la lista cerrada)
                if (!NeighborNode.IsWall || ClosedList.Contains(NeighborNode))
                {
                    continue; //Saltar ese vecino
                }
                //Calcula el coste F del vecino
                int MoveCost = CurrentNode.gCost + GetManHattenDistance(CurrentNode, NeighborNode);
                //Si el coste f es mayor que el coste g o si o esta en la lista abierta
                if (!OpenList.Contains(NeighborNode) || MoveCost < NeighborNode.FCost)
                    {
                    NeighborNode.gCost = MoveCost; //Cambia el coste g por el coste f
                    //Asigna el coste h
                    NeighborNode.hCost = GetManHattenDistance(NeighborNode, TargetNode);
                    //Asigna el padre para recorrer a la inversa la lista mas adelante
                    NeighborNode.Parent = CurrentNode;
                    //Si el vecino no esta en la lista
                    if (!OpenList.Contains(NeighborNode))
                    {
                        //Se añade
                        OpenList.Add(NeighborNode);
                    }
                }
            }
        }
        yield return null;
        //Si hemos econtrado el camino
        if (pathSuccess)
        {
           //Lo añadimos al array de waypoints
           waypoints = GetFinalPath(StartNode, TargetNode);
        }
        //Indicamos al requestManager que hemos terminado de hacer el pathfinding
        requestManager.FinishedProcessingPath(waypoints, pathSuccess);
    }

    //Esta fución devuelve un array de waypoints finales que forman el path
    Vector3[] GetFinalPath(Node a_StartingNode, Node a_EndNode)
    {
        List<Node> FinalPath = new List<Node>();
        Node CurrentNode = a_EndNode;

        //Añadimos cada uno de los padres a la lista final, desde el final al pricipio
        while (CurrentNode != a_StartingNode)
        {
            FinalPath.Add(CurrentNode);
            CurrentNode = CurrentNode.Parent;
        }
        //Este paso nos lo podemos saltar
        Vector3[] waypoints = SimplifyPath(FinalPath);
        //El camino invertido es el camino que el jugador tiene que recorrer
        Array.Reverse(waypoints);
        //Asigamos el recorrido final
        grid.FinalPath = FinalPath;
        //Devolvemos el recorrido final
        return waypoints;
    }

    //Esta función se encarga de simplificar el camino, pero para esta practica no la utilizaremos
    //Ya que queremos ver todo el camino que recorre
    Vector3[] SimplifyPath(List<Node> path)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 directionOld = Vector2.zero;
        for (int i = 1; i < path.Count; i++)
        {
            Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
            //Quitar si queremos tomar atajos en el path
            //if (directionNew != directionOld)
            //{
                waypoints.Add(path[i].Position);
            //}
            directionOld = directionNew;
        }
        return waypoints.ToArray();
    }

    //Calculo de la distanca Manhattan
    int GetManHattenDistance(Node a_nodeA, Node a_nodeB)
    {
        int ix = Mathf.Abs(a_nodeA.gridX - a_nodeB.gridX);
        int iy = Mathf.Abs(a_nodeA.gridY - a_nodeB.gridY);

        //Devolvemos el resultado de la operación
        return ix + iy;
    }
}