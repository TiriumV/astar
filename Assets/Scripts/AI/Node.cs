﻿using UnityEngine;

public class Node
{
    public int gridX; //Posición x en el array
    public int gridY; //Posición y en el array

    public bool IsWall; //Si ese nodo es circulable o no
    public Vector3 Position; //Posición en el mundo

    public Node Parent; //Padre del nodo, utilizado por el algoritmo    

    public int gCost; //Coste de moverse al siguiente cuadrado
    public int hCost; //Distancia al objetivo desde este nodo

    //Coste F (gCost + hCost)
    public int FCost { get { return gCost + hCost; } }

    public Node(bool a_IsWall, Vector3 a_Pos, int a_gridX, int a_gridY)
    {
        IsWall = a_IsWall;
        Position = a_Pos;
        gridX = a_gridX;
        gridY = a_gridY;
    }
}
