﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class PathRequestManager : MonoBehaviour
{
    //Creamos la cola de requests
    Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
    //La requests actual
    PathRequest currentPathRequest;

    //Creamos una instancia de requests
    static PathRequestManager instance;
    //referencia a la clase pathfinding
    Pathfinding pathfinding;
    //Booleana que nos indicara cuando terminamos el proceso
    bool isProcessingPath;

    private void Awake()
    {
        //Referenciamos las instancias
        instance = this;
        pathfinding = GetComponent<Pathfinding>();
    }

    //Creamos u delegate que almacenará en la cola la request
    //Ya que no vamos a dar respuesta inmediata a los requests.
    //Esta función devolverá el path final una vez este calculado
    public static void RequestPath(Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback)
    {
        //Creamos la request
        PathRequest newRequest = new PathRequest(pathStart, pathEnd, callback);
        //Metemos en la cola la request
        instance.pathRequestQueue.Enqueue(newRequest);
        //Itentamos saltar a la siguiete si esta ha terminado
        instance.TryProcessNext();
    }

    //determina si estamos procesando un path, si no es así, saltaremos a la siguiente request
    void TryProcessNext()
    {
        //Comprobamos si estamos procesando un path y si hay alguno mas que procesar
        if (!isProcessingPath && pathRequestQueue.Count > 0)
        {
            //Lo quitamos de la cola
            currentPathRequest = pathRequestQueue.Dequeue();
            //Cambiamos el booleano a "Esta procesando"
            isProcessingPath = true;
            //Empezamos a calcular el pathfinding
            pathfinding.StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd);
        }
    }

    //Comprueba si se ha terminado de procesar la request y pasa a la siguiete
    public void FinishedProcessingPath(Vector3[] path, bool success)
    {
        currentPathRequest.callback(path, success);
        isProcessingPath = false;
        TryProcessNext();
    }

    //Estructura del PathRequest
    struct PathRequest
    {
        public Vector3 pathStart;
        public Vector3 pathEnd;
        public Action<Vector3[], bool> callback;
        public PathRequest(Vector3 _start, Vector3 _end, Action<Vector3[], bool> _callback)
        {
            pathStart = _start;
            pathEnd = _end;
            callback = _callback;
        }
    }
}
