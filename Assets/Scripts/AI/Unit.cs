﻿using System.Collections;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public bool StartPathFinding = false; //Booleano que controla el loop del juego
    public Transform target; //Transform del objetivo a alcanzar
    public float speed = 5f; //Velocidad de movimiento
    Vector3[] path; //Array de posiciones del path
    int targetIndex; //Idex actual del array de path
    public Transform pathNode; //Prefab visual para ver por donde pasa el agente

    private void Update()
    {
        //Si el jugador pulsa espacio, empieza a buscar el camino y lo recorre
        if (Input.GetButton("Jump") && StartPathFinding == false)
        {
            //Hace una request al pathrequestmanager
            PathRequestManager.RequestPath(transform.position, target.position, OnpathFound);
            StartPathFinding = true;
        }
    }


    public void OnpathFound(Vector3[] newPath, bool pathSuccesfull)
    {
        //Si ha encontrado un camino, asigna el path
        if (pathSuccesfull)
        {
            path = newPath;
            //Detiene el seguimiento de otros paths
            StopCoroutine("FollowPath");
            //Empieza el nuevo
            StartCoroutine("FollowPath");
        }
        //Dibujamos el path por el que pasa el agente
        DrawPath();
    }

    //Este Enumerator se encarga de recorrer el path
    IEnumerator FollowPath()
    {
        Vector3 currentWaypoint = path[0];

        while (true)
        {
            //Si ha llegado al waypoint actual
            if(transform.position == currentWaypoint)
            {
                //Pasa al siguiente
                targetIndex++;
                //Si se pasa de largo para
                if (targetIndex >= path.Length)
                {
                    yield break;
                }
                //asigna el siguiente
                currentWaypoint = path[targetIndex];
            }
            //Movimiento por transform del jugador
            transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
            yield return null;
        }
    }

    //Fucion para la representacion visual del path
    public void DrawPath()
    {
        //Instanciamos un prefab en cada una de las posiciones del path
        for (int i = targetIndex; i < path.Length; i++)
        {
            Instantiate(pathNode, path[i], Quaternion.identity.normalized);
        }
    }
}
