﻿using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public LayerMask hitLayers;

    void Update()
    {
        if (Input.GetMouseButtonDown(1) && !GetComponent<Unit>().StartPathFinding)
        {
            Vector3 mouse = Input.mousePosition;
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity, hitLayers))
            {
               transform.position = hit.point;
            }
        }
    }
}